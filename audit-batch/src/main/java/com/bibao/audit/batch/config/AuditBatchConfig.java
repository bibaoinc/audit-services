package com.bibao.audit.batch.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.messaging.MessageHandler;

import com.bibao.audit.biz.config.AuditBizConfig;
import com.bibao.audit.model.AuditRecord;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableJms
@EnableIntegration
@IntegrationComponentScan
@Import(value = { AuditBizConfig.class, JmsConfig.class})
public class AuditBatchConfig {
	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = Jackson2ObjectMapperBuilder
						.json()
						.findModulesViaServiceLoader(true)
						.serializationInclusion(Include.NON_ABSENT)
						.build();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return mapper;
	}
	
	@Bean
	public Jackson2JsonObjectMapper jackson2JsonObjectMapper(ObjectMapper mapper) {
		return new Jackson2JsonObjectMapper(mapper);
	}
	
	@Bean 
	public JsonToObjectTransformer transformer(Jackson2JsonObjectMapper mapper) {
		 return new JsonToObjectTransformer(AuditRecord.class, mapper);
	} 

	@Bean
	public IntegrationFlow auditLisenerFlow(DefaultMessageListenerContainer msgListener,
			JsonToObjectTransformer transformer, 
			MessageHandler auditMessageHandler) {
		
		return IntegrationFlows
				.from(Jms.messageDrivenChannelAdapter(msgListener))
				.transform(transformer)
				.handle(auditMessageHandler)
				.get();
	}
	
}
