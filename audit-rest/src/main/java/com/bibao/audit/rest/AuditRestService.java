package com.bibao.audit.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;

import com.bibao.audit.publisher.AuditPublisher;
import com.bibao.audit.model.AuditRecord;
import com.bibao.audit.rest.contract.AuditRecordResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/audit")
@Api(value="Audit Services")
public class AuditRestService {
	@Autowired
	private AuditPublisher publisher;
	
	@POST
	@Path("/record")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@ApiOperation(value = "Publish Audit Record To Queue", response = AuditRecordResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Address standardized", response = AuditRecordResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Response.Status.class),
			@ApiResponse(code = 500, message = "Server exception", response = Response.Status.class)})
	public Response saveRecord(AuditRecord record) {
		publisher.publishMessage(() -> MessageBuilder.withPayload(record).build());
		AuditRecordResponse response = new AuditRecordResponse();
		response.setRecord(record);
		response.setMessage("Successfully saved the audit record");
		return Response.ok(response).build();
	}
}
