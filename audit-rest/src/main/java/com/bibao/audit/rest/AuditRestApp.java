package com.bibao.audit.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.audit.rest.AuditRestApp;
import com.bibao.audit.rest.config.AuditRestConfig;

@SpringBootApplication
@Import(AuditRestConfig.class)
public class AuditRestApp {
	
	public static void main(String[] args) {
		SpringApplication.run(AuditRestApp.class, args);
	}
}
