package com.bibao.audit.persistence.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "com.bibao.audit.persistence.dao")
@EnableTransactionManagement
@EntityScan(basePackages = {"com.bibao.audit.persistence.entity"})
@ComponentScan("com.bibao.audit.persistence")
public class JPAConfig {

}
