package com.bibao.audit.model;

public class AuditRecord {
	private String operation;
	private String oldValue;
	private String newValue;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
	@Override
	public String toString() {
		return "AuditRecord [operation=" + operation + ", oldValue=" + oldValue + ", newValue=" + newValue + "]";
	}
	
}
