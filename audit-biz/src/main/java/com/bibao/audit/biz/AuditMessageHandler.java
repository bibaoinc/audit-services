package com.bibao.audit.biz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bibao.audit.biz.mapper.AuditRecordMapper;
import com.bibao.audit.model.AuditRecord;
import com.bibao.audit.persistence.dao.AuditRecordDao;

@Component
public class AuditMessageHandler implements MessageHandler {
	private static final Logger LOG = LogManager.getLogger();
	
	@Autowired
	private AuditRecordDao auditRecordDao;
	
	@Override
	@Transactional
	public void handleMessage(Message<?> msg) {
		LOG.debug("Receiving message: " + msg.getPayload());
		AuditRecord auditRecord = (AuditRecord) msg.getPayload();
		auditRecordDao.save(AuditRecordMapper.mapToAuditRecordEntity(auditRecord));
	}

}
