package com.bibao.audit.publisher;

import java.util.function.Supplier;

import org.springframework.messaging.Message;

public interface AuditPublisher {
	public void publishMessage(Supplier<Message<?>>   msgSupplier) ;
}
